package com.jmcavoy;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.jmcavoy.Player.PlayerType;
import com.jmcavoy.pieces.Pawn;

import org.junit.jupiter.api.Test;

/**
 * Unit test for Player class
 */
public class PlayerTest {

    @Test
    void PiecesLookup() {
        Player player = new Player(PlayerType.WHITE);
        player.addPiece(new Coord(0, 0), new Pawn(PlayerType.WHITE));

        assertTrue(player.hasPiece(new Coord(0, 0)));
        assertFalse(player.hasPiece(new Coord(1, 0)));

        player.removePiece(new Coord(0, 0));

        assertFalse(player.hasPiece(new Coord(0, 0)));
    }
}