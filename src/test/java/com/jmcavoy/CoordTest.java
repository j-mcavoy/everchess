package com.jmcavoy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CoordTest {

    @Test
    void charConverstion() {
        Coord coord = new Coord(0, 0);
        Coord coordChar = new Coord('a', 1);

        assertEquals(coordChar, coord);
    }

}