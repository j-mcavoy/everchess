package com.jmcavoy.pieces;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.jmcavoy.Board;
import com.jmcavoy.Coord;
import com.jmcavoy.Move;
import com.jmcavoy.Player.PlayerType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit test for Pawn Piece
 */
class PawnTest {
    Board board;

    @BeforeEach
    void init() {
        board = new Board();
    }

    @Test
    void testPawnMoveForward() {
        Pawn pawn = new Pawn(PlayerType.WHITE);

        Coord pawnCoord = new Coord(1, 0);
        Coord forwardCoord = new Coord(2, 0);

        board.addPiece(pawn, pawnCoord);

        Move move = new Move(pawnCoord, forwardCoord);

        assertTrue(pawn.isMoveValid(board, move));
    }

    @Test
    void testPawnMoveAttackDiag() {
        Pawn pawn1 = new Pawn(PlayerType.WHITE);
        Pawn pawn2 = new Pawn(PlayerType.BLACK);

        Coord pawn1Coord = new Coord(1, 0);
        Coord pawn2Coord = new Coord(2, 1);

        board.addPiece(pawn1, pawn1Coord);
        board.addPiece(pawn2, pawn2Coord);

        Move attack1 = new Move(pawn1Coord, pawn2Coord);
        Move attack2 = new Move(pawn2Coord, pawn1Coord);

        assertTrue(pawn1.isMoveValid(board, attack1));
        assertTrue(pawn2.isMoveValid(board, attack2));
    }

    @Test
    void testPawnBlock() {
        Pawn pawn = new Pawn(PlayerType.WHITE);
        Pawn pawnBlock = new Pawn(PlayerType.WHITE);

        Coord pawnCoord = new Coord(1, 0);
        Coord pawnBlockCoord = new Coord(2, 0);
        Coord forwardCoord = new Coord(2, 0);

        board.addPiece(pawn, pawnCoord);
        board.addPiece(pawnBlock, pawnBlockCoord);

        Move move = new Move(pawnCoord, forwardCoord);

        assertFalse(pawn.isMoveValid(board, move));
    }
}
