package com.jmcavoy;

import java.util.Scanner;

import com.jmcavoy.Player.PlayerType;
import com.jmcavoy.pieces.Pawn;
import com.jmcavoy.pieces.Piece;

public final class Game {
    private PlayerType turn;
    private Board board;
    private Player white;
    private Player black;

    // Main program function Runs a new game, ends once the game is finished.
    public static void main(String[] args) {
        Game g = new Game();
        g.play();
    }

    public Game() {
        turn = PlayerType.WHITE;
        board = new Board();
        white = new Player(PlayerType.WHITE);
        black = new Player(PlayerType.BLACK);
    }

    // starts game loop, ends when game is over
    public void play() {
        resetBoard();
        while (!isGameOver()) {
            printBoard();
            Move move = promptMove();
            boolean isCapture = currentPlayer().hasCapture(board);
            executeMove(move);
            if (!isCapture) {
                switchTurn();
            }
        }
        System.out.println("Game over!");
    }

    // sets inital new game state
    private void resetBoard() {
        for (int row = 0; row < board.BOARD_HEIGHT; row++) {
            for (int col = 0; col < board.BOARD_WIDTH; col++) {
                if (row == 1) {
                    addPiece(new Pawn(PlayerType.WHITE), new Coord(row, col));
                } else if (row == board.BOARD_HEIGHT - 2) {
                    addPiece(new Pawn(PlayerType.BLACK), new Coord(row, col));
                } else {
                    addPiece(null, new Coord(row, col));
                }
            }
        }
    }

    // changes to the other player's turn
    private void switchTurn() {
        currentPlayer().onNewTurn();
        switch (turn) {
            case WHITE:
                turn = PlayerType.BLACK;
                break;
            case BLACK:
                turn = PlayerType.WHITE;
                break;
        }
    }

    // Returns the player whose turn it is
    private Player currentPlayer() {
        switch (turn) {
            case WHITE:
                return white;
            case BLACK:
                return black;
            default:
                return null;
        }
    }

    // Prompts player for next move, returns a valid move
    private Move promptMove() {
        Move move = new Move();
        Scanner scanner = new Scanner(System.in);
        while (!isMoveValid(move)) {
            try {
                System.out.println(currentPlayer() + ", Move from:");
                System.out.print("Col: ");
                char fromCol = scanner.next().charAt(0);
                System.out.print("Row: ");
                int fromRow = scanner.nextInt();
                Coord from = new Coord(fromCol, fromRow);

                System.out.println(currentPlayer() + ", Move to:");
                System.out.print("Col: ");
                char toCol = scanner.next().charAt(0);
                System.out.print("Row: ");
                int toRow = scanner.nextInt();
                Coord to = new Coord(toCol, toRow);

                move = new Move(from, to);
            } catch (Exception e) {
                scanner.next();
                printBoard();
                System.out.println(e);
                System.out.println("Invalid input!");
            }
            if (!isMoveValid(move)) {
                printBoard();
                System.out.println("Invalid move!");
            }
        }

        return move;

    }

    // Returns true if moving piece from one coordinate to another is valid
    private boolean isMoveValid(Move move) {
        if (!currentPlayer().hasPiece(move.from)) {
            return false;
        }
        Piece fromPiece = board.getPiece(move.from);
        if (currentPlayer().hasCapture(board)) {
            return fromPiece.hasCapture(board, move.from) && fromPiece.isMoveValid(board, move);
        }
        return fromPiece.isMoveValid(board, move);
    }

    // Returns true if there is no valid moves
    private boolean isGameOver() {
        if (!currentPlayer().hasMoves(board)) {
            return true;
        }
        for (int col = 0; col < board.BOARD_WIDTH; col++) {
            if (board.getPiece(new Coord(0, col)) != null
                    || board.getPiece(new Coord(board.BOARD_HEIGHT - 1, col)) != null) {
                return true;
            }
        }
        return false;
    }

    // Adds piece to board at coordinate and adds piece to the player's pieces list
    private void addPiece(Piece piece, Coord coord) {
        board.setPiece(piece, coord);
        if (piece != null) {
            switch (piece.getPlayerType()) {
                case WHITE:
                    white.addPiece(coord, piece);
                    break;
                case BLACK:
                    black.addPiece(coord, piece);
                    break;
            }
        }
    }

    // Removes piece from board and from player's pieces list
    private void removePiece(Coord coord) {
        Piece piece = board.getPiece(coord);
        if (piece != null) {
            switch (piece.getPlayerType()) {
                case WHITE:
                    white.removePiece(coord);
                case BLACK:
                    black.removePiece(coord);
            }
            board.setPiece(null, coord);
        }
    }

    // blindly executes move, call on capture if it's a capture
    private void executeMove(Move move) {
        Piece fromPiece = board.getPiece(move.from);
        if (currentPlayer().hasCapture(board)) {
            fromPiece.onCapture();
        }
        removePiece(move.to);
        removePiece(move.from);
        addPiece(fromPiece, move.to);
    }

    private void printBoard() {
        System.out.println(board.toString());
    }

}
