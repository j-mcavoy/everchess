package com.jmcavoy;

/**
 * Coordniate class for referencing chess board cell
 */
public class Coord {
    public int row, col;

    public Coord() {
        row = -1;
        col = -1;
    }

    public Coord(int row, int col) {
        this.row = row;
        this.col = col;
    }

    // create coord from board labels, ex: A2 = (0,1)
    public Coord(char colLabel, int rowLabel) {
        this.row = rowLabel - 1;
        this.col = charToCol(colLabel);
    }

    private int charToCol(char ch) {
        if (!Character.isAlphabetic(ch)) {
            return -1;
        }
        return Character.toLowerCase(ch) - 'a';
    }

    /**
     * Overide required for HashMap key
     */
    @Override
    public boolean equals(Object o) {
        Coord coord = (Coord) o;
        return coord.row == row && coord.col == col;
    }

    /**
     * Overide required for HashMap key
     */
    @Override
    public int hashCode() {
        String hashStr = row + "," + col;
        return hashStr.hashCode();
    }
}
