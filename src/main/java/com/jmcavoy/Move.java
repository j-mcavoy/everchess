package com.jmcavoy;

public class Move {

    public Coord from, to;

    public Move() {
        from = new Coord();
        to = new Coord();
    }

    public Move(Coord from, Coord to) {
        this.from = from;
        this.to = to;
    }

}