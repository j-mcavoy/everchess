package com.jmcavoy;

import com.jmcavoy.pieces.Piece;

public class Board {

    // board cell grid
    Cell[][] cells;
    // number of rows in the board
    public final int BOARD_HEIGHT = 8;
    // number of columns in the board
    public final int BOARD_WIDTH = 8;

    public Board() {
        cells = new Cell[BOARD_HEIGHT][BOARD_WIDTH];
        for (int row = 0; row < BOARD_HEIGHT; row++) {
            for (int col = 0; col < BOARD_HEIGHT; col++) {
                cells[row][col] = new Cell();
            }
        }
    }

    public void setPiece(Piece piece, Coord coord) {
        cells[coord.row][coord.col].setPiece(piece);
    }

    // gets piece at board coordinate
    public Piece getPiece(Coord coord) {
        return cells[coord.row][coord.col].getPiece();
    }

    // Adds a peice to the cell with coords pawnCoord
    public void addPiece(Piece piece, Coord coord) {
        cells[coord.row][coord.col].setPiece(piece);
    }

    @Override
    public String toString() {
        String str = "";
        for (int row = BOARD_HEIGHT - 1; row >= 0; row--) {
            str += (row + 1) + " ";
            for (int col = 0; col < BOARD_WIDTH; col++) {
                str += cells[row][col].getIcon() + " ";
            }
            str += "\n";
            if (row == 0) {
                str += "  ";
                for (int col = 0; col < BOARD_WIDTH; col++) {
                    str += (char) (col + 'a') + " ";
                }
            }

        }

        return str;
    }

    private class Cell {
        // piece occupying the cell
        private Piece piece;

        private Cell() {
            piece = null;
        }

        private Cell(Piece piece) {
            this.piece = piece;
        }

        private Piece getPiece() {
            return piece;
        }

        private void setPiece(Piece piece) {
            this.piece = piece;
        }

        // character icon that displays what is inside the cell
        private char getIcon() {
            if (piece == null) {
                return ' ';
            } else {
                return piece.getIcon();
            }
        }
    }
}
