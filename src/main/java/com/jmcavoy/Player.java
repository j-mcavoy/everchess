package com.jmcavoy;

import java.util.HashMap;
import java.util.Map;

import com.jmcavoy.pieces.Piece;

public class Player {

    public static enum PlayerType {
        WHITE, BLACK,
    };

    private HashMap<Coord, Piece> pieces;

    private PlayerType playerType;

    public Player(PlayerType playerType) {
        pieces = new HashMap<Coord, Piece>();
        this.playerType = playerType;
    }

    void addPiece(Coord coord, Piece piece) {
        pieces.put(coord, piece);
    }

    void removePiece(Coord coord) {
        pieces.remove(coord);
    }

    @Override
    public String toString() {
        switch (playerType) {
            case WHITE:
                return "White";
            case BLACK:
                return "Black";
            default:
                return "";
        }
    }

    // Returns true if player has a piece at the specified coordinate
    public boolean hasPiece(Coord coord) {
        return pieces.containsKey(coord);
    }

    public boolean hasMoves(Board board) {
        for (Map.Entry<Coord, Piece> entry : pieces.entrySet()) {
            Coord coord = entry.getKey();
            Piece piece = entry.getValue();
            if (piece.hasMoves(board, coord)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasCapture(Board board) {
        for (Map.Entry<Coord, Piece> entry : pieces.entrySet()) {
            Coord coord = entry.getKey();
            Piece piece = entry.getValue();
            if (piece.hasCapture(board, coord)) {
                return true;
            }
        }
        return false;
    }

    public void onNewTurn() {
        for (Map.Entry<Coord, Piece> entry : pieces.entrySet()) {
            Coord coord = entry.getKey();
            Piece piece = entry.getValue();
            piece.onNewTurn();
        }
    }
}