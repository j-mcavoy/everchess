package com.jmcavoy.pieces;

import com.jmcavoy.Board;
import com.jmcavoy.Coord;
import com.jmcavoy.Move;
import com.jmcavoy.Player.PlayerType;

public abstract class Piece {

    // player that the piece belongs to
    protected PlayerType playerType;

    // char represents piece, ex. p for pawn
    protected char icon;

    // either 1 or -1 depending on the player's perspective
    protected int forward;

    protected boolean hasCapturedThisRound;

    public Piece(PlayerType playerType) {
        this.playerType = playerType;
        hasCapturedThisRound = false;
        switch (playerType) {
            case WHITE:
                forward = 1;
                break;
            case BLACK:
                forward = -1;
                break;
        }
    }

    public void onCapture() {
        hasCapturedThisRound = true;
    }

    public void onNewTurn() {
        hasCapturedThisRound = false;
    }

    public abstract boolean hasMoves(Board board, Coord coord);

    public abstract boolean hasCapture(Board board, Coord coord);

    public abstract boolean isMoveValid(Board board, Move move);

    public char getIcon() {
        switch (playerType) {
            case WHITE:
                return Character.toLowerCase(icon);
            case BLACK:
                return Character.toUpperCase(icon);
            default:
                return ' ';
        }
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void setPlayerType(PlayerType playerType) {
        this.playerType = playerType;
    }

}