package com.jmcavoy.pieces;

import com.jmcavoy.Board;
import com.jmcavoy.Coord;
import com.jmcavoy.Move;
import com.jmcavoy.Player.PlayerType;

public class Pawn extends Piece {

    public Pawn(PlayerType playerType) {
        super(playerType);
        this.icon = 'p';
    }

    @Override
    public boolean hasCapture(Board board, Coord coord) {
        if (hasCapturedThisRound) {
            return false;
        }
        return isMoveValid(board, new Move(coord, new Coord(coord.row + forward, coord.col - 1)))
                || isMoveValid(board, new Move(coord, new Coord(coord.row + forward, coord.col + 1)));
    }

    @Override
    public boolean hasMoves(Board board, Coord coord) {
        return isMoveValid(board, new Move(coord, new Coord(coord.row + forward, coord.col)))
                || isMoveValid(board, new Move(coord, new Coord(coord.row + forward, coord.col - 1)))
                || isMoveValid(board, new Move(coord, new Coord(coord.row + forward, coord.col + 1)));
    }

    @Override
    public boolean isMoveValid(Board board, Move move) {
        // one move per round
        if (hasCapturedThisRound) {
            return false;
        }

        // null and out of bounds check
        if (move == null || move.from == null || move.to == null || move.to.row < 0 || move.to.row >= board.BOARD_HEIGHT
                || move.to.col < 0 || move.to.col >= board.BOARD_WIDTH) {
            return false;
        }

        final int dY = (move.to.row - move.from.row) * forward;
        final int dX = move.to.col - move.from.col;
        final Piece toPiece = board.getPiece(move.to);

        // attack diagonally
        if ((dX == 1 || dX == -1) && dY == 1) {
            return toPiece != null && toPiece.getPlayerType() != playerType;
        }

        // move forward
        if (dX == 0 && dY == 1 && !hasCapture(board, move.from)) {
            return toPiece == null;
        }
        return false;
    }

}