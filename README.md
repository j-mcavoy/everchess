# everchess

John McAvoy

## How to run
This project was made with Maven. You can run it from the commandline.
```
mvn exec:java -Dexec.mainClass=com.jmcavoy.Game
```

Or by importing the project into Eclipse as a Maven project and running it from there.

## How to play

White is lowercase letters and Black is uppercase.

### Moving pieces

1. Enter the coordinates to move from
2. Enter the coordinates to move to

example:

```
8
7 P P P P P P P P
6
5
4
3
2 p p p p p p p p
1
  a b c d e f g h

White, Move from:
Col: a
Row: 2
White, Move to:
Col: a
Row: 3

8
7 P P P P P P P P
6
5
4
3 p
2   p p p p p p p
1
  a b c d e f g h

```
